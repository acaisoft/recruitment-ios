iOS Developer - recruiting task

The task is to write a simple app which uses GitHub API (https://developer.github.com/v3/). The app should contain two screens:

1. Main screen: the list of user repositories and the search view.
2. Details screen: some additional data of the selected repository.


Application requirements

1. Main screen:

- As a user, you should be able to search the repositories by the GitHub username.
- Browsing is triggered by pressing the search button.
- If there are no repositories found for the specified username, an alert with the information should be displayed.
- If there are some repositories found, a list of these should be displayed. The list should contain:

	- Repository name
	- Repository description
	- Number of users that are watching this repository
	- Number of repository stars
	- Number of repository forks

![Main](Screens/main.png)
![No user](Screens/no_user.png)
![List](Screens/list.png)

2. Details screen:
After selecting an item from the list, a new screen should be pushed onto the navigation stack and some additional data should be fetched. After receiving the data, the following information should be displayed:

- The username, address (if available), email (if available) and the avatar.

![Details](Screens/details.png)

Additional info:

- Please create a project with Swift 5.
- Try using modern Swift approaches like CodingKeys for JSON mapping.
- Feel free to use the existing frameworks to speed up the development process.

Additional points (these are not obligatory, but will increase the value of project):

- Add some unit tests.
- Use some specific architecture pattern like MVVM/VIPER/etc.